﻿#include <iostream>
#include <fstream>
#include "Product.h"
#include "List.h"
using namespace std;

void menu()
{
	List<Product> store;
	store.ReadFromBinFile();
	cout << "Данные склада считаны." << endl;
	int menu = 0, index=0;
	while (true)
	{
		try
		{
			cout << "МЕНЮ"
				<< "\n1-Вывести на экран все товары на складе"
				<< "\n2-Добавить товар"
				<< "\n3-Удалить товар"
				<< "\n4-Изменить данные о товаре"
				<< "\n5-Отсортировать"
				<< "\n6-Найти товар"
				<< "\n7-Принять товар (считать с фактуры)"
				<< "\n8-Продать (создать фактуру)"
				<< "\n0-Выход" << endl;
			cin >> menu;
			if (menu < 0 || menu>8)
				throw 4;
			switch (menu)
			{
			case 1:
				store.print();
				break;
			case 2:
			{
				Product* NewProduct = new Product(menu);
				store.AddFront(NewProduct);
				cout << "Товар добавлен." << endl;
				store.SaveToBinFile();
				break;
			}
			case 3:
				store.print();
				cout << "Введите номер товара, который хотите удалить: ";
				cin >> index;
				store.DelIndex(index - 1);
				cout << "Товар удалён." << endl;
				store.SaveToBinFile();
				break;
			case 4:
				store.print();
				cout << "Введите номер товара, который хотите изменить: ";
				cin >> index;
				cout << "Какой парамер хотите изменить?"
					<< "\n1-Название"
					<< "\n2-Категория"
					<< "\n3-Количесвто"
					<< "\n4-Цена"
					<< "\n5-Надбавка" << endl;
				cin >> menu;
				switch (menu)
				{
				case 1:
					store[index - 1].SetName();
					break;
				case 2:
					store[index - 1].SetCategory();
					break;
				case 3:
					store[index - 1].SetNumber();
					break;
				case 4:
					store[index - 1].SetPrice();
					break;
				case 5:
					store[index - 1].SetMargin();
					break;
				}
				cout << "Данные изменены." << endl;
				store.SaveToBinFile();
				break;
			case 5:
				cout << "Сортировка..." << endl;
				store.sort();
				store.print();
				break;
			case 6:
				cout << "По какому параметру произвести поиск?"
					<< "\n1-Название"
					<< "\n2-Категория"
					<< "\n3-Количество" << endl;
				cin >> menu;
				switch (menu)
				{
				case 1:
					store.searchName();
					break;
				case 2:
					store.searchCategory();
					break;
				case 3:
					store.seachNumber();
					break;
				}
				break;
			case 7:
			{
				List<Product>* invoice = new List<Product>;
				invoice->ReadFromFile();
				invoice->NewTimeInvoice();
				store.listInclusion(invoice);
				cout << "Фактура считана, товар добавлен на склад." << endl;
				store.SaveToBinFile();
				break;
			}
				
			case 8:
			{
				List<Product>* invoice2 = new List<Product>;
				invoice2 = store.NewListInvoice();
				invoice2->SaveToFile();
				cout << "Счёт-фактура готова, общая торговая надбавка составила " <<
					invoice2->FullMargin() << " рублей." << endl;
				store.SaveToBinFile();
				break;
			}
				
			case 0:
				return;
			}
		}
		catch (int e)
		{
			if (e == 1)
				cout << "Ошибка! Не удалось открыть файл." << endl << endl;
			if (e == 2)
				cout << "Ошибка! Невозможно добавить или удалить элемент с указанным индексом." << endl << endl;
			if (e == 3)
				cout << "Ошибка! Список пуст." << endl << endl;
			if (e == 4)
				cout << "Ошибка! Выбран некорректный пункт меню." << endl << endl;
			if (e == 5)
				cout << "Ошибка! Не удалось обновить список товаров на складе. Данные могут быть утеряны." << endl << endl;
		}
	}
}

int main()
{
	setlocale(LC_ALL, "ru"); //Кстати, устанавливает не язык, а так называемую локаль: набор национ. парам. — кодировку, формат времени, валюту и пр. 
	menu();
	
	return 0;
}