﻿#pragma once
#include <iostream>
#include <fstream>
#include "Product.h"
#include <iomanip>
using namespace std;

template<class T>
class List
{
private:
	template<class T>
	class Node
	{
	public:
		Node* pNext;
		Node* pPrev;
		T* data;
		Node(T* data, Node* pNext = nullptr, Node* pPrev = nullptr)
		{
			this->data = data;
			this->pNext = pNext;
			this->pPrev = pPrev;
		}
		//Записать, почему не нужен конструктор копирования
	};

	int Size=0;
	Node<T>* head;
	Node<T>* tail;

public:
	List();
	~List();
	int GetSize() { return Size; };

	//Добавление элемента
	void AddFront(T* data);
	void AddIndex(T* value, int index);
	void AddBack(T* data);

	//Удаление элемента
	void DelFront();
	void DelIndex(int index);
	void DelBack();
	void clear(); //Удаление всего списка

	T& operator[](const int index); //Перегрузка оператора []

	void print(); //Вывод списка на экран

	void SaveToFile(); //Печать фактуры
	void ReadFromFile(); //Считывание фактуры
	int FullPrice(); //Подсчёт стоимости товара в фактуре
	int FullMargin(); //Подсчёт суммарной наценки по фактуре
	List<T>* NewListInvoice(); //Перенос товара из склада в фактуру
	void listInclusion(List<T>* invoice); //Перенос товара из фактуры на склад
	void NewTimeInvoice(); //Изменение даты поставки при считывании фактуры

	void SaveToBinFile(); //Сохранение в бинарный файл
	void ReadFromBinFile(); //Считывание из бинарного файла

	List<T>* sort(); //Сортировка

	void searchName(); //Поиск по названию
	void searchCategory(); //Поиск по категории
	void seachNumber(); //Поиск по количеству товара
};

template<class T>
List<T>::List()
{
	head = nullptr;
	tail = nullptr;
}

template<class T>
List<T>::~List()
{
	clear();
}

template<class T>
void List<T>::AddFront(T* data)
{
	if (Size == 0)
		head = tail = new Node<T>(data);
	else
	{
		Node<T>* temp = new Node<T>(data, head);
		head->pPrev = temp;
		head = temp;
	}
	Size++;
}

template<class T>
void List<T>::AddIndex(T* data, int index)
{
	if (index > Size || index < 0)
		throw 2;
	if (index == 0)
		AddFront(data);
	else
	{
		if (Size == 1 || Size == 2)
			AddBack(data);
		else
		{
			Node<T>* previous = this->head;
			for (int i = 0; i < index - 1; i++) //Находим элемент под нужным индексом
				previous = previous->pNext;
			Node<T>* newNode = new Node<T>(data, previous->pNext, previous);
			previous->pNext = newNode;
			newNode->pNext->pPrev = newNode;
			Size++;
		}
	}
}

template<class T>
void List<T>::AddBack(T* data)
{
	if (head == nullptr)
		head = tail = new Node<T>(data);
	else
	{
		Node<T>* temp = new Node<T>(data, nullptr, tail);
		tail->pNext = temp;
		tail = temp;
		if (Size == 1)
			head->pNext = temp;
	}
	Size++;
}

template<class T>
void List<T>::DelFront()
{
	if (Size == 0)
		throw 3;
	else
		if (Size == 1)
		{
			delete head;
			tail = nullptr;
			Size--;
		}
		else
		{
			Node<T>* temp = head;
			head = head->pNext;
			head->pPrev = nullptr;
			delete temp;
			Size--;
		}
}

template<class T>
void List<T>::DelIndex(int index)
{
	if (index > Size - 1 || index < 0)
		throw 2;
	else
	{
		if (index == 0)
			DelFront();
		else
			if (Size == index + 1)
				DelBack();
			else
			{
				Node<T>* previous = this->head;
				for (int i = 0; i < index - 1; i++)
					previous = previous->pNext;
				Node<T>* toDelete = previous->pNext;
				previous->pNext = toDelete->pNext;
				toDelete->pNext->pPrev = previous;
				delete toDelete;
				Size--;
			}
	}
}

template<class T>
void List<T>::DelBack()
{
	if (Size == 0)
		throw 3;
	else
	{
		if (Size == 1)
		{
			delete tail;
			head = nullptr;
			Size--;
		}
		else
		{
			Node<T>* temp = tail;
			tail = temp->pPrev;
			tail->pNext = nullptr;
			delete temp;
			Size--;
		}
	}
}

template<class T>
void List<T>::clear()
{
	while (Size != 0)
		DelFront();
}

template<class T>
inline T& List<T>::operator[](const int index)
{
	int counter = 0;
	T* temp=new T();
	Node<T>* current = head;
	while (current != nullptr)
	{
		if (counter == index)
			temp = current->data;
		current = current->pNext;
		counter++;
	}
	return *temp;
}

template<class T>
void List<T>::print()
{
	if (Size == 0)
		throw 3;
	else
		cout << setiosflags(ios::left);
		cout << "Список товаров" << endl << endl;
		cout << setw(4) << "#" << setw(15) << "Название" << setw(15) << "Категория" << setw(15) <<
			"Количество" << setw(10) << "Цена" << setw(15) <<
			"Наценка, %" << setw(10) << "Дата потупления" << endl;
		Node<T>* temp = head;
		for (int i=1; temp != nullptr; i++)
		{
			cout << setw(4) << i << *temp->data;
			temp = temp->pNext;
		}
		cout << endl;
}

template<class T>
void List<T>::SaveToFile()
{
	ofstream f("invoice.txt");
	if(!f)
		throw 1;
	f << setiosflags(ios::left);
	f << "ФАКТУРА" << endl;
	f << "Количество позиций: " << this->Size << endl;
	f << setw(15) << "Название" << setw(15) << "Категория" << setw(15) <<
		"Количество" << setw(10) << "Цена" << setw(15) <<
		"Наценка, %" << setw(10) << "Дата потупления" << endl;
	for (Node<T>* temp = head; temp != nullptr; temp = temp->pNext)
		f << *temp->data;
	f << "Полная сумма заказа " << this->FullPrice() << " рублей.";
	f.close();
	cout << "Список записан в файл" << endl;
}

template<class T>
inline void List<T>::ReadFromFile()
{
	ifstream f("invoice.txt");
	if(!f)
		throw 1;
	int SizeFile;
	f.ignore(8, '\n');
	f.ignore(20, '\n');
	f >> SizeFile;
	f.ignore(1, '\n');
	f.ignore(85, '\n');
	for (int i = 0; i < SizeFile; i++)
	{
		T* temp = new T("", "");
		f >> *temp;
		AddBack(temp);
	}
	f.close();
}

template<class T>
inline int List<T>::FullPrice()
{
	int summ = 0;
	for (Node<T>* temp = head; temp != nullptr; temp = temp->pNext)
		summ += temp->data->GetPrice()* temp->data->GetNumber();
	return summ;
}

template<class T>
inline int List<T>::FullMargin()
{
	int summ = 0;
	for (Node<T>* temp = head; temp != nullptr; temp = temp->pNext)
		summ += temp->data->GetPrice()* temp->data->GetNumber()* temp->data->GetMargin()/100;
	return summ;
}

template<class T>
inline List<T>* List<T>::NewListInvoice()
{
	List<T>* invoice = new List<T>;
	int menu, index, num;
	cout << "Создаём Фактуру." << endl;
	while (true)
	{
		cout << "Добавить новый товар?\n1-да\n2-нет" << endl;
		cin >> menu;
		if (menu != 1 && menu != 2)
			throw 4;
		if (menu == 1)
		{
			cout << "Товары на складе: ";
			this->print();
			cout << "Введите номер товара" << endl;
			cin >> index;
			cout << "Введите количество товара" << endl;
			cin >> num;
			T* temp = new T();
			*temp = this->operator[](index - 1);
			invoice->AddFront(temp); 
			invoice->head->data->SetNumber(num);
			invoice->print();
			this->operator[](index - 1).SetNumber(this->operator[](index - 1).GetNumber() - num);
		}
		else
			break;
	}
	return invoice;
}

template<class T>
inline void List<T>::listInclusion(List<T>* invoice)
{
	while (invoice->Size!=0)
	{
		AddFront(&invoice->operator[](0));
		invoice->DelFront();
	}
}

template<class T>
inline void List<T>::NewTimeInvoice()
{
	for (Node<T>* temp = head; temp != nullptr; temp = temp->pNext)
	{
		temp->data->SetTimeArrival();
	}
}

template<class T>
void List<T>::SaveToBinFile()
{
	fstream file;
	if(!file)
		throw 5;
	file.open("Store.txt", ios::out | ios::binary);
	file.write((char*)&this->Size, sizeof(this->Size));
	for (Node<T>* temp = head; temp != nullptr; temp = temp->pNext)
		temp->data->writeBin(file);
	file.close();
}

template<class T>
void List<T>::ReadFromBinFile()
{
	int SizeFile=0;
	fstream file;
	if (!file)
		throw 1;
	file.open("Store.txt", ios::in | ios::binary);
	file.read((char*)&SizeFile, sizeof(SizeFile));
	for (int i = 0; i < SizeFile;i++)
	{
		T* temp = new T("", "");
		temp->readBin(file);
		AddBack(temp);
	}
	file.close();
}

template<class T>
inline List<T>* List<T>::sort()
{
	for (int i = 1; i < Size; i++) 
	{
		for (int j = i; j > 0 && this->operator[](j - 1) > this->operator[](j); j--)
		{
			T temp = operator[](j - 1);
			operator[](j - 1) = operator[](j);
			operator[](j) = temp;
		}
	}

	if (Size == 0) 
		return this;
	return this;
}

template<class T>
inline void List<T>::searchName()
{
	cout << "Введите слово" << endl;
	char ArrTextName[256];
	SetConsoleCP(1251);
	cin.ignore(cin.rdbuf()->in_avail()); //Очистка буфера потока от оставшегося мусора. Извлекает из потока символы и отбрасывает их.
	cin.getline(ArrTextName, '\n');
	SetConsoleCP(866);
	char* text = new char[strlen(ArrTextName) + 1];
	strcpy_s(text, strlen(ArrTextName) + 1, ArrTextName);

	unsigned int start_time = clock();
	cout << "Начинаем поиск." << endl;
	cout << setw(15) << "Название" << setw(15) << "Категория" << setw(15) <<
		"Количество" << setw(10) << "Цена" << setw(15) <<
		"Наценка, %" << setw(10) << "Дата потупления" << endl;
	for (Node<T>* temp = head; temp!=nullptr; temp=temp->pNext)
		if (strcmp(temp->data->GetName(), text) == 0)
			cout << *temp->data << endl;
	cout << "Поиск окончен, результаты выведены на экран." << endl;
	unsigned int stop_time = clock() - start_time;
	cout << "Поиск занял " << stop_time << "мс" << endl;
	delete[] text;
}

template<class T>
inline void List<T>::searchCategory()
{
	cout << "Введите слово" << endl;
	char ArrTextCategory[256];
	SetConsoleCP(1251);
	cin.ignore(cin.rdbuf()->in_avail()); //Очистка буфера потока от оставшегося мусора. Извлекает из потока символы и отбрасывает их.
	cin.getline(ArrTextCategory, '\n');
	SetConsoleCP(866);
	char* text = new char[strlen(ArrTextCategory) + 1];
	strcpy_s(text, strlen(ArrTextCategory) + 1, ArrTextCategory);

	unsigned int start_time = clock();
	cout << "Начинаем поиск:" << endl;
	cout << setw(15) << "Название" << setw(15) << "Категория" << setw(15) <<
		"Количество" << setw(10) << "Цена" << setw(15) <<
		"Наценка, %" << setw(10) << "Дата потупления" << endl;
	for (Node<T>* temp = head; temp != nullptr; temp = temp->pNext)
		if (strcmp(temp->data->GetCategory(), text) == 0)
			cout << *temp->data << endl;
	cout << "Поиск окончен, результаты выведены на экран." << endl;
	unsigned int stop_time = clock() - start_time;
	cout << "Поиск занял " << stop_time << "мс" << endl;
	delete[] text;
}

template<class T>
inline void List<T>::seachNumber()
{
	int searchNumberMenu=0;
	int tempNumber;
	cout << "Какой товар искать?" <<
		"\n1-Больше указанного количества" <<
		"\n2-Меньше указанного количества" << endl;
	cin >> searchNumberMenu;
	if (searchNumberMenu != 1 && searchNumberMenu != 2)
		throw 4;
	cout << "Какое количество?" << endl;
	cin >> tempNumber;

	unsigned int start_time = clock();
	cout << "Начинаем поиск:" << endl;
	cout << setw(15) << "Название" << setw(15) << "Категория" << setw(15) <<
		"Количество" << setw(10) << "Цена" << setw(15) <<
		"Наценка, %" << setw(10) << "Дата потупления" << endl;
	switch (searchNumberMenu)
	{
	case 1:
		for (Node<T>* temp = head; temp != nullptr; temp = temp->pNext)
		{
			if (temp->data->GetNumber() > tempNumber)
				cout << *temp->data;
		}
		break;
	case 2:
		for (Node<T>* temp = head; temp != nullptr; temp = temp->pNext)
		{
			if (temp->data->GetNumber() < tempNumber)
				cout << *temp->data;
		}
		break;
	}
	cout << "Поиск окончен, результаты выведены на экран." << endl;
	unsigned int stop_time = clock() - start_time;
	cout << "Поиск занял " << stop_time << "мс" << endl;
}
