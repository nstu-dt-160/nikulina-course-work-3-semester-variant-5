﻿#pragma once
#include<iostream>
#include <Windows.h>
using namespace std;

class Product
{
private:
	char* name = nullptr;     //Наименование товара
	char* category = nullptr; //Категория 
	char timeArrival[11];     //Дата поступления              //Пояснить размер: в конце добавляется завершающий символ
	int number;				  //Количество 
	int price;				  //Цена
	int margin;				  //Торговая надбавка/наценка
public:
	Product();
	Product(int a);
	Product(const Product& other);
	Product(const char* name, const char* category, int number = 0, int price = 0, int margin = 0)
	{
		if (name)
		{
			this->name = new char[strlen(name) + 1];
			strcpy_s(this->name, strlen(name) + 1, name);
		}
		if (category)
		{
			this->category = new char[strlen(category) + 1];
			strcpy_s(this->category, strlen(category) + 1, category);
		}
		this->number = number;
		this->price = price;
		this->margin = margin;
		SYSTEMTIME time;
		GetLocalTime(&time);
		sprintf_s(timeArrival, "%02d.%02d.%04d", time.wDay, time.wMonth, time.wYear);
	};
	~Product();

	//Сеттеры
	void SetName();
	void SetCategory();
	void SetNumber();
	void SetNumber(int number);
	void SetPrice();
	void SetMargin();
	void SetTimeArrival();

	//Геттеры
	char* GetName();
	char* GetCategory();
	int GetNumber();
	int GetPrice();
	int GetMargin();

	friend ostream& operator<< (ostream& os, Product& p); //Вывод на экран
	friend istream& operator>> (istream& is, Product& p); //Ввод с консоли
	friend ofstream& operator<< (ofstream& os, Product& p); //Запись в файл
	friend ifstream& operator>> (ifstream& is, Product& p); //Считывание из файла
	void writeBin(fstream& stream); //Запись в бинарный файл
	void readBin(fstream& stream); //Чтение из бинарного файла

	void operator=(const Product& other); //Перегрузка оператора присваивания
	//friend bool operator<(Product& a, Product& b); //Перегрузка оператора сравнения
	friend bool operator>(Product& a, Product& b); //Перегрузка оператора сравнения
	bool compareDate(Product& a, Product& b); //Сравнение дат
};