﻿#include "Product.h"
#include <fstream>
#include <iomanip> 

Product::Product()
{
	this->name = new char[strlen("") + 1];
	strcpy_s(this->name, strlen("") + 1, "");
	this->category = new char[strlen("") + 1];
	strcpy_s(this->category, strlen("") + 1, "");
	this->number = 0;
	this->price = 0;
	this->margin = 0;
	SYSTEMTIME time;
	GetLocalTime(&time);
	sprintf_s(timeArrival, "%02d.%02d.%04d", time.wDay, time.wMonth, time.wYear);
}

Product::Product(int a)
{
	//Можно не выделять память заранее, так как там по умолчанию хранится nullptr
	SetName();
	SetCategory();
	SetNumber();
	SetPrice();
	SetMargin();
	SYSTEMTIME time;
	GetLocalTime(&time);
	sprintf_s(timeArrival, "%02d.%02d.%04d", time.wDay, time.wMonth, time.wYear);
}

Product::Product(const Product& other)
{
	if (other.name)
	{
		this->name = new char[strlen(other.name) + 1];
		strcpy_s(this->name, strlen(other.name) + 1, other.name);
	}
	if (other.category)
	{
		this->category = new char[strlen(other.category) + 1];
		strcpy_s(this->category, strlen(other.category) + 1, other.category);
	}
	this->number = other.number;
	this->price = other.price;
	this->margin = other.margin;
	for (int i = 0; i < 11; i++)
	{
		this->timeArrival[i] = other.timeArrival[i];
	}
}

Product::~Product()
{
	delete[] name;
	delete[] category;
}

void Product::writeBin(fstream& stream)
{
	if (!stream.is_open()) 
		throw 1;

	size_t length = strlen(this->name);
	stream.write((char*)&length, sizeof(length));
	stream.write(this->name, length);

	length = strlen(this->category);
	stream.write((char*)&length, sizeof(length));
	stream.write(this->category, length);

	stream.write((char*)&this->number, sizeof(this->number));
	stream.write((char*)&this->price, sizeof(this->price));
	stream.write((char*)&this->margin, sizeof(this->margin));

	stream.write(this->timeArrival, sizeof(this->timeArrival));
}

void Product::readBin(fstream& stream)
{
	if (!stream.is_open()) 
		throw 1;

	size_t length=0;

	delete[] this->name;
	stream.read((char*)&length, sizeof(length));
	this->name = new char[length + 1];  
	stream.read(this->name, length);
	this->name[length] = '\0'; //Строка обязательно заканчивается \0

	delete[] this->category;
	stream.read((char*)&length, sizeof(length));
	this->category = new char[length + 1];
	stream.read(this->category, length);
	this->category[length] = '\0';

	stream.read((char*)&this->number, sizeof(this->number));
	stream.read((char*)&this->price, sizeof(this->price));
	stream.read((char*)&this->margin, sizeof(this->margin));

	stream.read(this->timeArrival, sizeof(this->timeArrival));
}

void Product::operator=(const Product& other)
{
	if (other.name)
	{
		this->name = new char[strlen(other.name) + 1];
		strcpy_s(this->name, strlen(other.name) + 1, other.name);
	}
	if (other.category)
	{
		this->category = new char[strlen(other.category) + 1];
		strcpy_s(this->category, strlen(other.category) + 1, other.category);
	}
	this->number = other.number;
	this->price = other.price;
	this->margin = other.margin;
	for (int i = 0; i < sizeof(this->timeArrival); i++)
		this->timeArrival[i] = other.timeArrival[i];
}

bool Product::compareDate(Product& a, Product& b)
{
	if (a.timeArrival[8] > b.timeArrival[8])
		return true;
	if (a.timeArrival[9] > b.timeArrival[9])
		return true;
	if (a.timeArrival[3] > b.timeArrival[3])
		return true;
	if (a.timeArrival[4] > b.timeArrival[4])
		return true;
	if (a.timeArrival[0] > b.timeArrival[0])
		return true;
	if (a.timeArrival[1] > b.timeArrival[1])
		return true;
	return false;
}

void Product::SetName()
{
	cout << "Введите название товара: " << endl;
	char name[256] = {0};
	SetConsoleCP(1251);
	cin.ignore(cin.rdbuf()->in_avail()); //Очистка буфера потока от оставшегося мусора. Извлекает из потока символы и отбрасывает их.
	cin.getline(name,256, '\n');
	SetConsoleCP(866);
	delete[] this->name;
	this->name = new char[strlen(name) + 1];
	strcpy_s(this->name, strlen(name) + 1, name); //Проверка не нужна, потому что я всегда что-то ввожу с консоли
}

void Product::SetCategory()
{
	cout << "Введите категорию товара: " << endl;
	char category[256];
	SetConsoleCP(1251);
	cin.ignore(cin.rdbuf()->in_avail()); //Очистка буфера потока от оставшегося мусора. Извлекает из потока символы и отбрасывает их.
	cin.getline(category,256, '\n');
	SetConsoleCP(866);
	delete[] this->category;
	this->category = new char[strlen(category) + 1];
	strcpy_s(this->category, strlen(category) + 1, category);
}

void Product::SetNumber()
{
	cout << "Введите количество товара: " << endl;
	cin >> number;
}

void Product::SetNumber(int number)
{
	this->number = number;
}

void Product::SetPrice()
{
	cout << "Введите цену: " << endl;
	cin >> price;
}

void Product::SetMargin()
{
	cout << "Введите наценку в %: " << endl;
	cin >> margin;
}

void Product::SetTimeArrival()
{
	SYSTEMTIME time;
	GetLocalTime(&time);
	sprintf_s(timeArrival, "%02d.%02d.%04d", time.wDay, time.wMonth, time.wYear);
}

char* Product::GetName()
{
	return name;
}

char* Product::GetCategory()
{
	return category;
}

int Product::GetNumber()
{
	return number;
}

int Product::GetPrice()
{
	return price;
}

int Product::GetMargin()
{
	return margin;
}

ostream& operator<<(ostream& os, Product& p)
{
	cout << setiosflags(ios::left);
	cout << setw(15) << p.name << setw(15) << p.category << setw(15) <<
		p.number << setw(10) << p.price << setw(15) << p.margin <<
		setw(10) << p.timeArrival << endl;
	return os;
}

istream& operator>>(istream& is, Product& p)
{
	p.SetName();
	p.SetCategory();
	p.SetNumber();
	p.SetPrice();
	p.SetMargin();
	return is;
}

ofstream& operator<<(ofstream& os, Product& p)
{
	if (!os)
		throw 1;
	else
	{
		os << setiosflags(ios::left);
		os << setw(15) << p.name << setw(15) << p.category << setw(15) <<
			p.number << setw(10) << p.price << setw(15) << p.margin <<
			setw(10) << p.timeArrival << endl;
	}
	return os;
}

ifstream& operator>>(ifstream& is, Product& p)
{
	if (!is)
		throw 1;
	else
	{
		char text[256];
		is >> text;
		delete p.name;
		p.name = new char[strlen(text) + 1];
		strcpy_s(p.name, strlen(text) + 1, text);
		is.ignore(1, '\n');
		is >> text;
		delete p.category;
		p.category = new char[strlen(text) + 1];
		strcpy_s(p.category, strlen(text) + 1, text);
		is.ignore(1, '\n');
		is >> p.number;
		is.ignore(1, '\n');
		is >> p.price;
		is.ignore(1, '\n');
		is >> p.margin;
		is.ignore(1, '\n');
		is >> p.timeArrival;
	}
	return is;
}

bool operator>(Product& a, Product& b)
{
	if (strcmp(a.name, b.name) > 0)
		return true;
	if (strcmp(a.name, b.name) < 0)
		return false;
	if (strcmp(a.category, b.category) > 0)
		return true;
	if (strcmp(a.category, b.category) < 0)
		return false;
	if (a.number > b.number)
		return true;
	if (a.number < b.number)
		return false;
	if (a.price > b.price)
		return true;
	if (a.price < b.price)
		return false;
	if (a.margin > b.margin)
		return true;
	if (a.margin < b.margin)
		return false;
	if (a.compareDate(a,b))
		return true;
	return false;
}


